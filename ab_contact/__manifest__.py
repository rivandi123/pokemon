# -*- coding: utf-8 -*-
{
    'name': "ab_contact",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "https://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/16.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','contacts','report_xlsx'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/contact_views.xml',
        'views/views.xml',
        'views/templates.xml',
        'report/report_pokemon_action.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'assets': {
    'web.assets_backend': [
       'ab_contact/static/src/js/contact_tree.js',
       'ab_contact/static/src/js/contact_kanban.js',
       'ab_contact/static/src/xml/contact_tree.xml',
       'ab_contact/static/src/xml/contact_kanban.xml',
   ]
},
}

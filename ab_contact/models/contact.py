from odoo import models, fields, api, _
import requests

class ResPartner(models.Model):
    _inherit = 'res.partner'

    url = fields.Char('Url')
    hp = fields.Char('HP')
    attack = fields.Char('Attack')
    defence = fields.Char('defence')
    spec_att = fields.Char('Special Attack')
    spec_def = fields.Char('Special Defence')
    speed = fields.Char('Speed')
    pokemon_ids = fields.Many2many('pokemon.types', string='PokemonTypes')

    def get_pokemon(self):
        url = "https://pokeapi.co/api/v2/pokemon"
        response = requests.get(url)

        if response.status_code == 200:
            pokemon_data = response.json()["results"]

            for pokemon in pokemon_data:
                partner = self.env["res.partner"]
                existing_pokemon = partner.search([("name", "=", pokemon["name"])])

                if not existing_pokemon:
                    partner.create({
                        "name": pokemon["name"],
                        "url": pokemon["url"],
                    })
                    print(f"{pokemon['name']} ditambahkan.")
                else:
                    print(f"{pokemon['name']} sudah ada.")
        else:
            print(f"Error: {response.status_code}")

    def get_pokemon_api(self):
        for pokemon in self:
            if pokemon.url:
                try:
                    response = requests.get(pokemon.url)
                    if response.status_code == 200:
                        pokemon_data = response.json()
                        pokemon.hp = pokemon_data["stats"][0]["base_stat"]
                        pokemon.attack = pokemon_data["stats"][1]["base_stat"]
                        pokemon.defence = pokemon_data["stats"][2]["base_stat"]
                        pokemon.spec_att = pokemon_data["stats"][3]["base_stat"]
                        pokemon.spec_def = pokemon_data["stats"][4]["base_stat"]
                        pokemon.speed = pokemon_data["stats"][5]["base_stat"]
                        pokemon_types = []
                        for type in pokemon_data["types"]:
                            existing_type = self.env["pokemon.types"].search([("name_type", "=", type["type"]["name"])])

                            if not existing_type:
                                new_type = self.env["pokemon.types"].create({
                                    "name_type": type["type"]["name"],
                                    "url_type": type["type"]["url"]
                                })
                                pokemon_types.append(new_type.id)
                            else:
                                pokemon_types.append(existing_type.id)

                        pokemon.pokemon_ids = [(6, 0, pokemon_types)]
                except requests.RequestException as e:
                    print("Error:", e)


class PokemonTypes(models.Model):
    _name = 'pokemon.types'

    name_type = fields.Char('Name')
    url_type = fields.Char('url')

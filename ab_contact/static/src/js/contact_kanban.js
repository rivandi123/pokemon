/** @odoo-module */
import { KanbanController } from "@web/views/kanban/kanban_controller";
import { registry } from '@web/core/registry';
import { kanbanView } from '@web/views/kanban/kanban_view';
export class ResPartnerButtonKanbanController extends KanbanController {
    setup() {
        super.setup();
    }
    OnFetchClick() {
        var rpc = require('web.rpc');
        rpc.query({
            model: 'res.partner',
            method: 'get_pokemon',
            args: [""]
        }).then();
    }
    OnPrintClick() {
        this.actionService.doAction("ab_contact.report_pokemon_action")
    }
}
registry.category("views").add("button_in_kanban", {
    ...kanbanView,
    Controller: ResPartnerButtonKanbanController,
    buttonTemplate: "res_partner.kanbanView.Buttons",
});
/** @odoo-module */
import { ListController } from "@web/views/list/list_controller";
import { registry } from '@web/core/registry';
import { listView } from '@web/views/list/list_view';
export class ResPartnerButtonListController extends ListController {
    setup() {
        super.setup();
    }
    OnFetchClick() {
        var rpc = require('web.rpc');
        rpc.query({
            model: 'res.partner',
            method: 'get_pokemon',
            args: [""]
        }).then();
    }
    OnPrintClick() {
        this.actionService.doAction("ab_contact.report_pokemon_action")
    }
}
registry.category("views").add("button_in_tree", {
    ...listView,
    Controller: ResPartnerButtonListController,
    buttonTemplate: "res_partner.listView.Buttons",
});
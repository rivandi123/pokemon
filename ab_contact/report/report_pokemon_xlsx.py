from odoo import models, fields, api

class ReportPokemonXlsx(models.AbstractModel):
    _name = 'report.ab_contact.report_pokemon_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        sheet = workbook.add_worksheet('Pokemon API')

        header_style = workbook.add_format({
            'font_size': 12,
            'bold': True,
            'valign': 'vcenter',
            'text_wrap': True,
            'border': 1
        })
        text_style = workbook.add_format({
            'font_size': 12,
            'valign': 'vcenter',
            'text_wrap': True,
            'border': 1
        })
        sheet.set_column('A:H', 20)

        header = [
            'Nama Pokemon',
            'Base HP',
            'Base Attack',
            'Base Defense',
            'Base Special Attack',
            'Base Special Defence',
            'Base Speed',
            'Type',
        ]

        sheet.write_row(0, 0, header, header_style)
        row = 1
        for pokemon in self.env['res.partner'].search([]):
            data = [
                pokemon.name,
                pokemon.hp or '',
                pokemon.attack or '',
                pokemon.defence or '',
                pokemon.spec_att or '',
                pokemon.spec_def or '',
                pokemon.speed or '',
                "/".join([type.name_type for type in pokemon.pokemon_ids]),
            ]
            sheet.write_row(row, 0, data, text_style)
            row += 1
